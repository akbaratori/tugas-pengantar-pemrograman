import java.util.*;
public class Matrikss {
	static Scanner scan=new Scanner(System.in);
	static int pilihan;
	static void MainMenu(){
		while(true){
			int pilih;
			System.out.println("      ====================================");
			System.out.println("\t\t   Menu Matriks");
			System.out.println("      ====================================\n");			
			System.out.println("1. Penjumlahan");
			System.out.println("2. Pengurangan");
			System.out.println("3. Perkalian");
			System.out.println("0. Keluar\n");			
			System.out.println("      ====================================\n");
			System.out.print("Masukkan Nomor Pilihan  : ");
			do{
				try{
					pilih=scan.nextInt();
					if(pilih!= 1 && pilih!= 2 && pilih!= 3  && pilih!= 0 ){
						System.out.println("Masukkan pilihan yang tersedia");
						continue;
					}
					break;
				}catch(InputMismatchException e){
					System.out.println("Masukkan pilihan yang tersedia");
					scan.nextLine();
					continue;
				}
			}while(true);
			if(pilih== 0){
				System.exit(0);
			}
			switch(pilih){
				case 1:
					penjumlahan();
					break;
				case 2:
					pengurangan();
					break;
				case 3:
					perkalian();
					break;	
			}	
		}
	}
	
                                         //////////////////////PENJUMLAHAN//////////////////////
	
	static void penjumlahan(){
		int baris,kolom;
		do{
			System.out.println("       ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
			System.out.println("\t       Matriks Penjumlahan");
			System.out.println("       ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
			do{	
				try{				
					System.out.print("\n\tMasukkan nilai untuk baris matriks : ");
					baris=scan.nextInt();
						if (baris <1){
							throw new InputMismatchException();
						}
					System.out.print("\n\tMasukkan nilai untuk kolom matriks : ");
					kolom=scan.nextInt();
						if (kolom <1){
							throw new InputMismatchException();
						}
					break;
				}catch(InputMismatchException e){
					System.out.println("Masukkan angka lebih besar dari 0");
					scan.nextLine();
					continue;
				}
			}while(true);			
			double[][]matriks1= new double[baris][kolom];
			double[][]matriks2= new double[baris][kolom];
			double[][]hasil=new double[baris][kolom];
			System.out.println("\t\t =====================");
			System.out.println("\t\t       Matriks A");
			System.out.println("\t\t =====================\n");
			do{
				try{
					for(int indeksbaris=0; indeksbaris<baris;indeksbaris++){
						for(int indekskolom=0; indekskolom<kolom;indekskolom++){
							System.out.print("Masukkan nilai baris "+(indeksbaris+1)+" dan Kolom "+(indekskolom+1)+" : ");
							matriks1[indeksbaris][indekskolom]=scan.nextDouble();
						}
					}
					break;
				}catch(InputMismatchException e){
					System.out.println("Masukkan bilangan real");
					scan.nextLine();
					continue;
				}	
			}while(true);
			System.out.println();
			for(int indeksbaris=0; indeksbaris<baris;indeksbaris++){
				for(int indekskolom=0; indekskolom<kolom;indekskolom++){
					System.out.printf("%.2f  ",matriks1[indeksbaris][indekskolom]);
				}System.out.println();
			}
			System.out.println();
			System.out.println("\t\t =====================");
			System.out.println("\t\t       Matriks B");
			System.out.println("\t\t =====================\n");
			do{
				try{
					for(int indeksbaris=0; indeksbaris<baris;indeksbaris++){
						for(int indekskolom=0; indekskolom<kolom;indekskolom++){
							
							/////////////////TANDAIII///////////////
							
							System.out.print("Masukkan nilai baris "+(indeksbaris+1)+" dan Kolom "+(indekskolom+1)+" : ");
							
							matriks2[indeksbaris][indekskolom]=scan.nextDouble();
							hasil[indeksbaris][indekskolom]=matriks1[indeksbaris][indekskolom] + matriks2[indeksbaris][indekskolom];
						}
					}
					break;
				}catch(InputMismatchException e){
					System.out.println("Masukkan bilangan real");
					scan.nextLine();
					continue;
				}	
			}while(true);
			
			System.out.println();
			for(int indeksbaris=0; indeksbaris<baris;indeksbaris++){
				for(int indekskolom=0; indekskolom<kolom;indekskolom++){
					
					System.out.printf("%.2f  ",matriks2[indeksbaris][indekskolom]);
				}System.out.println();
			}			
			System.out.println("\n");
			System.out.println("Hasil dari penjumlahan kedua matriks adalah : ");
			System.out.println();
			for(int indeksbaris=0; indeksbaris<baris;indeksbaris++){
				for(int indekskolom=0; indekskolom<kolom;indekskolom++){
					
					System.out.printf("\t\t %.2f  ",hasil[indeksbaris][indekskolom]);
				}System.out.println("\n");
			}
			if(baris != kolom){
				System.out.println("Matriks tersebut tidak memiliki determinan");
			}	
			else{
				
				if(baris==2 && kolom==2){
					if(((hasil[0][0]*hasil[1][1])-(hasil[0][1]*hasil[1][0]))!=0){
						System.out.println("Determinan dari matriks tersebut adalah "+((hasil[0][0]*hasil[1][1])-(hasil[0][1]*hasil[1][0])));
					}
					else{
						System.out.println("Matriks tersebut merupakan matriks pencerminan");
					}
				}
				else if(baris==3 && kolom==3){
					if(((matriks2[0][0]*((matriks2[1][1]*matriks2[2][2])-(matriks2[2][1]*matriks2[1][2])))
							-(matriks2[0][1]*((matriks2[1][0]*matriks2[2][2])-(matriks2[2][0]*matriks2[1][2])))
							+(matriks2[0][2]*((matriks2[1][0]*matriks2[2][1])-(matriks2[2][0]*matriks2[1][1]))))!=0){
						System.out.println("Determinan dari matriks tersebut adalah "+((matriks2[0][0]*((matriks2[1][1]*matriks2[2][2])-(matriks2[2][1]*matriks2[1][2])))
								-(matriks2[0][1]*((matriks2[1][0]*matriks2[2][2])-(matriks2[2][0]*matriks2[1][2])))
								+(matriks2[0][2]*((matriks2[1][0]*matriks2[2][1])-(matriks2[2][0]*matriks2[1][1])))));
					}
					else{
						System.out.println("Matriks tersebut merupakan matriks pencerminan");
					}
				}
			}	
			System.out.println();
			scan.nextInt();
			do{
				System.out.println("\t   =========MENU==========");
				System.out.println("Tekan 1 Untuk ulangi dan 0 untuk keluar");
				pilihan=scan.nextInt();
				if (pilihan==0){
					 break;
				 }
				 else  if (pilihan==1){
					 break;
				 }
				 else{
					 System.out.println("Masukkan angka 1 atau 0");
					 continue;
				 }
			}while(true);
			 if (pilihan==0){
				 break;
			 }
			 else  if (pilihan==1){
				 continue;
			 }
		}while(true);		
	}
	
	                                    //////////////////////PENGURANGAN//////////////////////
	
	static void pengurangan(){
		int baris,kolom;
		do{
			System.out.println("       ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
			System.out.println("\t         Matriks Pengurangan");
			System.out.println("       ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");	
			do{	
				try{
					
					System.out.print("\nMasukkan jumlah baris matriks : ");
					baris=scan.nextInt();
						if (baris <1){
							throw new InputMismatchException();
						}
					System.out.print("Masukkan jumlah kolom matriks : ");
					kolom=scan.nextInt();
						if (kolom <1){
							throw new InputMismatchException();
						}
					break;
				}catch(InputMismatchException e){
					System.out.println("Masukkan angka lebih besar dari 0");
					scan.nextLine();
					continue;
				}
			}while(true);	
			double[][]matriks1= new double[baris][kolom];
			double[][]matriks2= new double[baris][kolom];
			double[][]hasil=new double[baris][kolom];
			System.out.println("\t\t =====================");
			System.out.println("\t\t       Matriks A");
			System.out.println("\t\t =====================\n");
			do{
				try{
					for(int indeksbaris=0; indeksbaris<baris;indeksbaris++){
						for(int indekskolom=0; indekskolom<kolom;indekskolom++){
							System.out.print("Masukkan nilai baris "+(indeksbaris+1)+" dan Kolom "+(indekskolom+1)+" : ");
							
							matriks1[indeksbaris][indekskolom]=scan.nextDouble();
						}
					}
					break;
				}catch(InputMismatchException e){
					System.out.println("Masukkan bilangan real");
					scan.nextLine();
					continue;
				}	
			}while(true);			
			System.out.println();
			for(int indeksbaris=0; indeksbaris<baris;indeksbaris++){
				for(int indekskolom=0; indekskolom<kolom;indekskolom++){
					
					System.out.printf("%.2f  ",matriks1[indeksbaris][indekskolom]);
				}System.out.println();
			}	
			System.out.println();		
			System.out.println("\t\t =====================");
			System.out.println("\t\t       Matriks B");
			System.out.println("\t\t =====================\n");
			do{
				try{
					for(int indeksbaris=0; indeksbaris<baris;indeksbaris++){
						for(int indekskolom=0; indekskolom<kolom;indekskolom++){
							System.out.print("Masukkan nilai baris "+(indeksbaris+1)+" dan Kolom "+(indekskolom+1)+" : ");
							
							matriks2[indeksbaris][indekskolom]=scan.nextDouble();
							hasil[indeksbaris][indekskolom]= matriks1[indeksbaris][indekskolom] - matriks2[indeksbaris][indekskolom];
						}
					}
					break;
				}catch(InputMismatchException e){
					System.out.println("Masukkan bilangan real");
					scan.nextLine();
					continue;
				}	
			}while(true);		
			System.out.println();
			for(int indeksbaris=0; indeksbaris<baris;indeksbaris++){
				for(int indekskolom=0; indekskolom<kolom;indekskolom++){
					
					System.out.printf("%.2f  ",matriks2[indeksbaris][indekskolom]);
				}System.out.println();
			}		
			System.out.println("\n");
			System.out.println("Hasil pengurangan kedua matriks tersebut adalah : ");
			System.out.println();	
			for(int indeksbaris=0; indeksbaris<baris;indeksbaris++){
				for(int indekskolom=0; indekskolom<kolom;indekskolom++){
					
					System.out.printf("\t\t %.2f  ",hasil[indeksbaris][indekskolom]);
				}System.out.println("\n");
			}
			if(baris != kolom){
				System.out.println("Matriks tersebut tidak memiliki determinan");
			}	
			else{
				
				if(baris==2 && kolom==2){
					if(((hasil[0][0]*hasil[1][1])-(hasil[0][1]*hasil[1][0]))!=0){
						System.out.println("Determinan dari matriks tersebut adalah "+((hasil[0][0]*hasil[1][1])-(hasil[0][1]*hasil[1][0])));
					}
					else{
						System.out.println("Matriks tersebut merupakan matriks pencerminan");
					}
				}
				else if(baris==3 && kolom==3){
					if(((matriks2[0][0]*((matriks2[1][1]*matriks2[2][2])-(matriks2[2][1]*matriks2[1][2])))
							-(matriks2[0][1]*((matriks2[1][0]*matriks2[2][2])-(matriks2[2][0]*matriks2[1][2])))
							+(matriks2[0][2]*((matriks2[1][0]*matriks2[2][1])-(matriks2[2][0]*matriks2[1][1]))))!=0){
						System.out.println("Determinan dari matriks tersebut adalah "+((matriks2[0][0]*((matriks2[1][1]*matriks2[2][2])-(matriks2[2][1]*matriks2[1][2])))
								-(matriks2[0][1]*((matriks2[1][0]*matriks2[2][2])-(matriks2[2][0]*matriks2[1][2])))
								+(matriks2[0][2]*((matriks2[1][0]*matriks2[2][1])-(matriks2[2][0]*matriks2[1][1])))));
					}
					else{
						System.out.println("Matriks tersebut merupakan matriks pencerminan");
					}
				}
			}
			System.out.println();
			scan.nextLine();
			do{
				System.out.println("\t   =========MENU==========");
				System.out.println("Tekan 1 untuk ulangi dan 0 untuk keluar");
				pilihan=scan.nextInt();
				if (pilihan==0){
					 break;
				 }
				 else  if (pilihan==1){
					 break;
				 }
				 else{
					 System.out.println("Masukkan angka 1 atau 0");
					 continue;
				 }
			}while(true);
			
				 if (pilihan==0){
					 break;
				 }
				 else  if (pilihan==1){
					 continue;
				 }		
		}while(true);	
	}
	
                                      //////////////////////PERKALIAN//////////////////////
	
	static void perkalian(){	
		while(true){
			 int matriksMultiTotal=0,pilihan=0,baris1,kolom1,baris2,kolom2;
		 do{
			 do{
				 try{
					 System.out.print("Jumlah baris matriks A: ");
					 baris1=scan.nextInt();
					 System.out.print("Jumlah kolom matriks A: ");
					 kolom1=scan.nextInt();
					 System.out.print("Jumlah baris matriks B: ");
					 baris2=scan.nextInt();
					 System.out.print("Jumlah kolom matriks B: ");
					 kolom2=scan.nextInt();
					 break;
				 }catch(InputMismatchException e){
					 System.out.println("Masukkan bilangan bulat");
					 scan.nextLine();
					 continue;
				 }
			 }while(true); 
					if (kolom1 != baris2) {
						System.out.println("Matriks tidak dapat dikalikan");					
						do{
							try{
								System.out.println("\t   =========MENU==========");
								System.out.print("Tekan 1 untuk ulangi dan 0 untuk keluar : ");
								pilihan=scan.nextInt();
								if (pilihan==0)break;
								if (pilihan==1)break;
							}catch(InputMismatchException e){
								System.out.println("Masukkan pilihan yang tersedia");
								scan.nextLine();
								continue;
							}
						}while((pilihan!=0)||(pilihan!=1));
					}			
					if (pilihan==0)break;
					if (pilihan==1)continue;
		 }while(kolom1!=baris2);
			 if (pilihan==1)break;
			 System.out.println("\n"); 
		int[][]matriks=new int[baris1][kolom1];
		do{
			System.out.println("Matriks A: ");
			for(int indeksbaris=0;indeksbaris<baris1;indeksbaris++){
				for(int indekskolom=0;indekskolom<kolom1;indekskolom++){
					System.out.print("Baris "+(indeksbaris+1)+" Kolom "+(indekskolom+1)+" : ");
					try{
						matriks[indeksbaris][indekskolom]=scan.nextInt();
					}catch(InputMismatchException e){
						System.out.println("Masukkan bilangan bulat");
						scan.nextLine();
						indekskolom--;
						continue;
					}	
				}
			}
			break;
		}while(true);		
		System.out.println();	
		for(int indeksbaris=0;indeksbaris<baris1;indeksbaris++){
			for(int indekskolom=0;indekskolom<kolom1;indekskolom++){
				System.out.printf("%4d ", matriks[indeksbaris][indekskolom]);
			}System.out.println();
		}System.out.println();
		int[][]matriks2=new int[baris2][kolom2];
		do{
			System.out.println("Matriks B: ");
			for(int indeksbaris=0;indeksbaris<baris2;indeksbaris++){
				for(int indekskolom=0;indekskolom<kolom2;indekskolom++){
					System.out.print("row "+(indeksbaris+1)+" Kolom "+(indekskolom+1)+" : ");
					try{
						matriks2[indeksbaris][indekskolom]=scan.nextInt();
					}catch(InputMismatchException e){
						System.out.println("Masukkan bilangan bulat");
						scan.nextLine();
						indekskolom--;
						continue;
					}
				}
			}
			break;
		}while(true);	
		System.out.println();	
		for(int indeksbaris=0;indeksbaris<baris2;indeksbaris++){
			for(int indekskolom=0;indekskolom<kolom2;indekskolom++){
				System.out.printf("%4d ", matriks2[indeksbaris][indekskolom]);
			}System.out.println();
		}	
		int[][]matriks3=new int[baris1][kolom2];
		for(int indeksbaris=0;indeksbaris<baris1;indeksbaris++){
			for(int indekskolom=0;indekskolom<kolom2;indekskolom++){
				matriksMultiTotal=0;
				for(int perkalianindex=0;perkalianindex<kolom1;perkalianindex++){
					matriksMultiTotal+=matriks[indeksbaris][perkalianindex]*matriks2[perkalianindex][indekskolom];
				}
				matriks3[indeksbaris][indekskolom]=matriksMultiTotal;
					}
				}System.out.println();		
		System.out.println("Perkalian dari matriks "+baris1+"x"+kolom1+" X "+baris2+"x"+kolom2+" Akan membuat matriks "+baris1+"x"+kolom2);
		System.out.println("hasil dari perkalian kedua matriks adalah : ");
		for(int indeksbaris=0;indeksbaris<baris1;indeksbaris++){
			for(int indekskolom=0;indekskolom<kolom2;indekskolom++){
				System.out.printf("%4d ", matriks3[indeksbaris][indekskolom]);
			}
			System.out.println();
		}		
		System.out.println("\n");		
		if(baris1 != kolom2){
			System.out.println("Matriks tersebut tidak memiliki determinan");
		}	
		else{
			if(baris1==2 && kolom2==2){
				if(((matriks3[0][0]*matriks3[1][1])-(matriks3[0][1]*matriks3[1][0]))!=0){
					System.out.println("Determinan dari matriks tersebut adalah "+((matriks3[0][0]*matriks3[1][1])-(matriks3[0][1]*matriks3[1][0])));
				}
				else{
					System.out.println("Matriks tersebut merupakan matriks pencerminan");
				}
			}
			else if(baris1==3 && kolom2==3){
				if(((matriks2[0][0]*((matriks2[1][1]*matriks2[2][2])-(matriks2[2][1]*matriks2[1][2])))
						-(matriks2[0][1]*((matriks2[1][0]*matriks2[2][2])-(matriks2[2][0]*matriks2[1][2])))
						+(matriks2[0][2]*((matriks2[1][0]*matriks2[2][1])-(matriks2[2][0]*matriks2[1][1]))))!=0){
					
					System.out.println("Determinan dari matriks tersebut adalah "+((matriks2[0][0]*((matriks2[1][1]*matriks2[2][2])-(matriks2[2][1]*matriks2[1][2])))
							-(matriks2[0][1]*((matriks2[1][0]*matriks2[2][2])-(matriks2[2][0]*matriks2[1][2])))
							+(matriks2[0][2]*((matriks2[1][0]*matriks2[2][1])-(matriks2[2][0]*matriks2[1][1])))));
				}
				else{
					System.out.println("Matriks tersebut merupakan matriks pencerminan");
				}
			}
		}		
			do{
				try{
					System.out.print("Tekan 1 untuk ulangi dan 0 untuk keluar : ");
					pilihan=scan.nextInt();
					if (pilihan==0)break;
					if (pilihan==1)break;
				}catch(InputMismatchException e){
					System.out.println("Masukkan pilihan yang tersedia");
					scan.nextLine();
					continue;
				}		
			}while((pilihan<0)||(pilihan>1));
			if (pilihan==0)break;
			if(pilihan==1)continue;
			System.out.println();
		 }
	}
	
	public static void main(String[] args){
			MainMenu();
    }	
}
